#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 4444

//-------------------------FÜGGVÉNYDEKLARÁCIÓK---------------------------------------------------


//FÜGGVÉNY AMI KIRAJZOLJA A TÁBLÁT

void print_table(char t1[],char t2[],char t3[],char t4[],char t5[],char t6[],char t7[]){
	
printf("     _____________________________\n");  
printf("     |(%c)|(%c)|(%c)|(%c)|(%c)|(%c)|(%c)|\n",t1[0],t2[0],t3[0],t4[0],t5[0],t6[0],t7[0]);
printf("     |---------------------------|\n");
printf("     |(%c)|(%c)|(%c)|(%c)|(%c)|(%c)|(%c)|\n",t1[1],t2[1],t3[1],t4[1],t5[1],t6[1],t7[1]);
printf("     |---------------------------|\n");
printf("     |(%c)|(%c)|(%c)|(%c)|(%c)|(%c)|(%c)|\n",t1[2],t2[2],t3[2],t4[2],t5[2],t6[2],t7[2]);
printf("     |---------------------------|\n");
printf("     |(%c)|(%c)|(%c)|(%c)|(%c)|(%c)|(%c)|\n",t1[3],t2[3],t3[3],t4[3],t5[3],t6[3],t7[3]);
printf("     |---------------------------|\n");
printf("     |(%c)|(%c)|(%c)|(%c)|(%c)|(%c)|(%c)|\n",t1[4],t2[4],t3[4],t4[4],t5[4],t6[4],t7[4]);
printf("     |---------------------------|\n");
printf("     |(%c)|(%c)|(%c)|(%c)|(%c)|(%c)|(%c)|\n",t1[5],t2[5],t3[5],t4[5],t5[5],t6[5],t7[5]);
printf("     |---------------------------|\n");
printf("     |				 |\n");
printf("    /|				/|\n");
}

//FÜGGVÉNY AMI FELTÖLTI A TÁBLÁT SZÓKÖZÖKKEL


void nulling(char t[])
	{
		for(int i=0;i<6;i++){
			t[i]=' ';
		}
	}

//-----------------------------------------------------------------------------------------------	

//---------------------------------KLIENS KIÉPÍTÉSE----------------------------------------------

int main(){


	int network_socket, ret;
	struct sockaddr_in server_address;


	network_socket = socket(AF_INET,SOCK_STREAM,0);

	memset(&server_address, '\0',sizeof(server_address));
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(PORT);
	server_address.sin_addr.s_addr = INADDR_ANY;

	int player_socket = accept(network_socket,NULL,NULL);

	ret = connect(network_socket,(struct sockaddr*)&server_address, sizeof(server_address));
	if(ret<0){
		printf("Csatlakozási hiba\n");
		exit(1);
	}
	printf("Sikeresen csatlakoztál a másik játékosra várunk.\n");
//-----------------------------------------------------------------------------------------------	

//-------------------------------------ELŐKÉSZÜLETEK-----------------------------------------------	

	//EXTRA EREDMÉNY KIIRATÁSA EGY FÁJLBA
	FILE *f = fopen("eredmenyek.txt", "a");
	if (f == NULL)
	{
   	 	printf("Nincs ilyen fájl!\n");
    	exit(1);
	}

	//TÁBLA 
	char oszlop1[6];
	char oszlop2[6];
	char oszlop3[6];
	char oszlop4[6];
	char oszlop5[6];
	char oszlop6[6];
	char oszlop7[6];

	//TÁBLA KINULLÁZÁSA
	nulling(oszlop1),nulling(oszlop2),nulling(oszlop3),nulling(oszlop4),nulling(oszlop5),nulling(oszlop6),nulling(oszlop7);

	int result; //EREDMÉNYT TÁROLÓ VÁLTOZÓ 

	//ÜDVÖZLŐ ÜZENET FOGADÁSA
	char msg_udv[256];
	recv(network_socket,&msg_udv,sizeof(msg_udv),0);
	printf("Szerver: %s",msg_udv);
//-----------------------------------------------------------------------------------------------	

//-------------------------------------JÁTÉK-----------------------------------------------	
	
for(;;){

//--------------------------------OSZLOP BEKÉRÉSE--------------------------------------------------
	//INSTRUKCIÓS ÜZENET FOGADÁSA
	char msg_input[256];
	recv(network_socket,&msg_input,sizeof(msg_input),0);
	printf("Szerver: %s",msg_input);

	//KIVÁLASZOTT OSZLOP BEÍRÁSA
	char input[10];
	scanf("%s",input);

	//HIBAKEZELÉS - ROSSZ OSZLOP TARTOMÁNY

	while((strcmp(input,"1")!=0)&&(strcmp(input,"2")!=0)&&(strcmp(input,"3")!=0)&&(strcmp(input,"4")!=0&&(strcmp(input,"5")!=0)
	&&(strcmp(input,"6")!=0)&&(strcmp(input,"7")!=0)&&(strcmp(input,"feladom")!=0))){
	printf("Hibás oszlopszámot adtál meg! Adj meg 1-7 között értéket, feladáshoz írd: feladom\n");
	scanf("%s",input);
	}

	//HIBAKZEZLÉS - AZ OSZLOP TELE VAN-------------------------------------------------------------------

	if((strcmp(input,"1")==0)){
		while(oszlop1[0]!=' '){
		printf("Az oszlop betelt, adj meg egy másikat\n");
		scanf("%s",input);
		if(strcmp(input,"1")!=0){
			break;
		}
		}
	}

	if((strcmp(input,"2")==0)){
		while(oszlop2[0]!=' '){
		printf("Az oszlop betelt, adj meg egy másikat\n");
		scanf("%s",input);
		if(strcmp(input,"2")!=0){
			break;
		}
		}
	}

	if((strcmp(input,"3")==0)){
		while(oszlop3[0]!=' '){
		printf("Az oszlop betelt, adj meg egy másikat\n");
		scanf("%s",input);
		if(strcmp(input,"3")!=0){
			break;
		}
		}
	}

	if((strcmp(input,"4")==0)){
		while(oszlop4[0]!=' '){
		printf("Az oszlop betelt, adj meg egy másikat\n");
		scanf("%s",input);
		if(strcmp(input,"4")!=0){
			break;
		}
		}
	}


	if((strcmp(input,"5")==0)){
		while(oszlop5[0]!=' '){
		printf("Az oszlop betelt, adj meg egy másikat\n");
		scanf("%s",input);
		if(strcmp(input,"5")!=0){
			break;
		}
		}
	}

	if((strcmp(input,"6")==0)){
		while(oszlop6[0]!=' '){
		printf("Az oszlop betelt, adj meg egy másikat\n");
		scanf("%s",input);
		if(strcmp(input,"6")!=0){
			break;
		}
		}
	}

	if((strcmp(input,"7")==0)){
		while(oszlop7[0]!=' '){
		printf("Az oszlop betelt, adj meg egy másikat\n");
		scanf("%s",input);
		if(strcmp(input,"7")!=0){
			break;
		}
		}
	}

//----------------------------------------------------------------------------------------------	
	printf("Várakozás a másik játékosra.\n");
	//A MEGADOTT OSZLOP ELKÜLDÉSE A SZERVERNEK
	send(network_socket,input,sizeof(input),0);

	

//------------------------------------------------------------------------------------------------	

//--------------------------------TÁBLA ÁLLAPOTÁNAK FOGADÁSA,KIRAJZOLÁS---------------------------

	recv(network_socket,&oszlop1,sizeof(oszlop1),0);
	recv(network_socket,&oszlop2,sizeof(oszlop2),0);
	recv(network_socket,&oszlop3,sizeof(oszlop4),0);
	recv(network_socket,&oszlop4,sizeof(oszlop4),0);
	recv(network_socket,&oszlop5,sizeof(oszlop5),0);
	recv(network_socket,&oszlop6,sizeof(oszlop6),0);
	recv(network_socket,&oszlop7,sizeof(oszlop7),0);


	print_table(oszlop1,oszlop2,oszlop3,oszlop4,oszlop5,oszlop6,oszlop7);


//------------------------------------------------------------------------------------------------


//--------------------------------------VALAMELYIK JÁTÉKOS NYERT-------------------------------------

	recv(network_socket,&result,sizeof(result),0);
	if(result!=0){
		if(result==1){
		printf("Player1 nyert!\n");
		fprintf(f,"Az első játékos nyert!\n");
		fclose(f);
		exit(1);
		}
		else
		{
			if(result==2){
				printf("Player2 nyert!\n");
				fprintf(f,"Az második játékos nyert!\n");
				fclose(f);
				exit(1);
			}
			else{
				if(result==3){
					printf("Döntetlen!\n");
					fprintf(f,"Az eredmény döntetlen!\n");
					fclose(f);
					exit(1);
				}
			}
		}

	}

}

//------------------------------------------------------------------------------------------------


	return 0;
}