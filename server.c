#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "put.h"
#include "win.h"

#define PORT 4444

//-------------------------FÜGGVÉNYDEKLARÁCIÓK-------------------------------------------


	//FÜGGVÉNY AMI FELTÖLTI A TÁBLÁT SZÓKÖZÖKKEL

	void nulling(char t[])
	{
		for(int i=0;i<6;i++){
			t[i]=' ';
		}
	}

//-----------------------------------------------------------------------------------------------	

//---------------------------------SZERVER KIÉPÍTÉSE----------------------------------------------

	int main(){

	int binding;
	int player1_socket;
	int player2_socket;

	//SZERVER SOCKET LÉTREHOZÁSA

	int server_socket;
	server_socket = socket(AF_INET,SOCK_STREAM,0);



	//ADDRESS

	struct sockaddr_in server_address;
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(PORT);
	server_address.sin_addr.s_addr = INADDR_ANY;

	//BIND

	binding = bind(server_socket,(struct sockaddr*)&server_address,sizeof(server_address));
	if(binding!=0){
		printf("A porthoz való csatlakozás sikertelen\n");
		exit(1);
	}
	else{
		printf("A szerver elindult.\n");
		printf("Várakozás a Játékosok csatlakozására...\n");
	}


	listen(server_socket, 4);
//-----------------------------------------------------------------------------------------------

//---------------------------------------ELŐKÉSZÜLETEK----------------------------------------------

	//JÁTÉKOS STRUKTÚRA 

	typedef struct jatekos{
		int socket;
		char ertek[1];
		char oszlop[10];
	} JATEKOS;



	char p1_ertek; 				//A PLAYER1 JÁTÉKOS ÉRTÉKE (0)
	char p2_ertek;				//A PLAYER2 JÁTÉKOS ÉRTÉKE (X)
	int result=0;				//A JÁTÉK ÁLLAPOTÁT TARTALMAZÓ VÁLTOZÓ
	int sor_num;				//A SOROKBAN TÖRTÉNŐ NYERÉS MEGNÉZÉSÉHEZ HASZNÁLT VÁLTOZÓ
	

	//TÁBLA

	char oszlop1[6];
	char oszlop2[6];
	char oszlop3[6];
	char oszlop4[6];
	char oszlop5[6];
	char oszlop6[6];
	char oszlop7[6];

	//TÁBLA FELTÖLTÉSE SZÓKÖZÖKKEL

	nulling(oszlop1),nulling(oszlop2),nulling(oszlop3),nulling(oszlop4),nulling(oszlop5),nulling(oszlop6),nulling(oszlop7);

	//DÖNTETLEN BEMUTATÁSRA
	//oszlop1[0]='0';
	//oszlop2[0]='x';
	//oszlop3[0]='x';
	//oszlop4[0]='0';
	//oszlop5[0]='x';
	//oszlop6[0]='x';
	//oszlop7[0]='0';


	//A KÉT JÁTÉKOS STRUKTÚRÁNAK LEFOGLALJUK A MEMÓRIÁT

	JATEKOS* player1 = malloc(sizeof(JATEKOS));
	JATEKOS* player2 = malloc(sizeof(JATEKOS));

	//A PLAYER1 ÉRTÉKE 0

	strcpy(player1->ertek,"0");
	p1_ertek='0';


	//A PLAYER2 ÉRTÉKE X

	strcpy(player2->ertek,"x");
	p2_ertek='x';


	//KÉT JÁTÉKOS SOCKET CSATLAKOZÁSA,JÁTÉKOT KEZDŐ ÜZENET KÜLDÉSE

	player1->socket = accept(server_socket,NULL,NULL);
	printf("player1 csatlakozott\n");
	player2->socket = accept(server_socket,NULL,NULL);
	printf("player2 csatlakozott\n");

	char msg_udv[256]={"Mindkét játékos sikeresen csatlakozott kezdődhet a játék.\n"};
	send(player1->socket,msg_udv,sizeof(msg_udv),0);
	send(player2->socket,msg_udv,sizeof(msg_udv),0);

//-----------------------------------------------------------------------------------------------

//---------------------------------------JÁTÉK----------------------------------------------------

for(;;){

//---------------------------------------OSZLOP KIVÁLASZTÁSA,FOGADÁSA-------------------------------

	//INSTRUKCIÓS ÜZENET KÜLDÉSE
	char msg_input[256]={"Add meg az oszlopot ahová szeretnéd ejeteni a korongot!\n"};
	send(player1->socket,msg_input,sizeof(msg_input),0);
	send(player2->socket,msg_input,sizeof(msg_input),0);


	char input[10];


	//PLAYER1 VÁLASZTÁSA

	recv(player1->socket,&input,sizeof(input),0);
	strcpy(player1->oszlop,input);

	//FELADÁS ÉS DÖNTETLEN ELLENŐRZÉSE

	if(strcmp(player1->oszlop,"feladom")==0){
		result=2; 
	}

	dontetlen(oszlop1,oszlop2,oszlop3,oszlop4,oszlop5,oszlop6,oszlop7,&result);

	//-----------------------------------ELLENŐRIZNI NYERT-E VALAKI--------------------------------------

	win_oszlop_all(oszlop1,oszlop2,oszlop3,oszlop4,oszlop5,oszlop6,oszlop7,&result,p1_ertek);
	win_sor_all(oszlop1,oszlop2,oszlop3,oszlop4,oszlop5,oszlop6,oszlop7,&result,sor_num);
	win_atlo_all(oszlop1,oszlop2,oszlop3,oszlop4,oszlop5,oszlop6,oszlop7,&result);
	putting_value(oszlop1,oszlop2,oszlop3,oszlop4,oszlop5,oszlop6,oszlop7,player1->oszlop,p1_ertek);

	

//---------------------------------------------------------------------------------------------------	

	//KIIRATJUK A SZERVER ÁLLAPOTÁT 0-SEMMI,1-JÁTÉKOS1 NYERT,2-JÁTÉKOS2 NYERT,3-DÖNTETLEN
	//KIIRATJUK MIT VÁLASZTOTT A JÁTÉKOS1

	printf("Szerver állapota: %d\n",result);
	printf("Az első játékos választása:%s\n",player1->oszlop);


	//PLAYER2 VÁLASZTÁSA
	
	recv(player2->socket,&input,sizeof(input),0);
	strcpy(player2->oszlop,input);

	//FELADÁS ÉS DÖNTETLEN ELLENŐRZÉSE

	if(strcmp(player2->oszlop,"feladom")==0){
		result=1; 
	}

	
	putting_value(oszlop1,oszlop2,oszlop3,oszlop4,oszlop5,oszlop6,oszlop7,player2->oszlop,p2_ertek);


//-----------------------------------ELLENŐRIZNI NYERT-E VALAKI--------------------------------------

	win_oszlop_all(oszlop1,oszlop2,oszlop3,oszlop4,oszlop5,oszlop6,oszlop7,&result,p2_ertek);
	win_sor_all(oszlop1,oszlop2,oszlop3,oszlop4,oszlop5,oszlop6,oszlop7,&result,sor_num);
	win_atlo_all(oszlop1,oszlop2,oszlop3,oszlop4,oszlop5,oszlop6,oszlop7,&result);
	dontetlen(oszlop1,oszlop2,oszlop3,oszlop4,oszlop5,oszlop6,oszlop7,&result);
	

//---------------------------------------------------------------------------------------------------	

	//KIIRATJUK A SZERVER ÁLLAPOTÁT 0-SEMMI,1-JÁTÉKOS1 NYERT,2-JÁTÉKOS2 NYERT,3-DÖNTETLEN
	//KIIRATJUK MIT VÁLASZTOTT A JÁTÉKOS1

	printf("Szerver állapota: %d\n",result);
	printf("A második játékos választása:%s\n",player2->oszlop);
	

//-------------------------------------------------------------------------------------------------	

//-------------------------------TÁBLA ÁLLAPOTÁNAK KÜLDÉSE-----------------------------------------

	send(player1->socket,oszlop1,sizeof(oszlop1),0);
	send(player1->socket,oszlop2,sizeof(oszlop2),0);
	send(player1->socket,oszlop3,sizeof(oszlop3),0);
	send(player1->socket,oszlop4,sizeof(oszlop4),0);
	send(player1->socket,oszlop5,sizeof(oszlop5),0);
	send(player1->socket,oszlop6,sizeof(oszlop6),0);
	send(player1->socket,oszlop7,sizeof(oszlop7),0);


	send(player2->socket,oszlop1,sizeof(oszlop1),0);
	send(player2->socket,oszlop2,sizeof(oszlop2),0);
	send(player2->socket,oszlop3,sizeof(oszlop3),0);
	send(player2->socket,oszlop4,sizeof(oszlop4),0);
	send(player2->socket,oszlop5,sizeof(oszlop5),0);
	send(player2->socket,oszlop6,sizeof(oszlop6),0);
	send(player2->socket,oszlop7,sizeof(oszlop7),0);


//----------------------------------------VAN-E NYERTES-------------------------------------------------	

	send(player1->socket,&result,sizeof(result),0);
	send(player2->socket,&result,sizeof(result),0);

}



	return 0;
}






	
	
